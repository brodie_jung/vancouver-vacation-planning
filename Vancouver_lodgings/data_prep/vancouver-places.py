# Extract Spark-style JSON from planet.osm data.
# Typical invocation:
# spark-submit osm-amenities.py /courses/datasets/openstreetmaps amenities

import sys
assert sys.version_info >= (3, 5) # make sure we have Python 3.5+

from pyspark.sql import SparkSession, functions, types, Row
spark = SparkSession.builder.appName('OSM point of interest extracter').getOrCreate()
assert spark.version >= '2.4' # make sure we have Spark 2.4+
spark.sparkContext.setLogLevel('WARN')
sc = spark.sparkContext
spark.conf.set("spark.sql.session.timeZone", "UTC")

from lxml import etree

amenity_schema = types.StructType([
    types.StructField('lat', types.DoubleType(), nullable=False),
    types.StructField('lon', types.DoubleType(), nullable=False),
    types.StructField('amenity', types.StringType(), nullable=True),
    types.StructField('tourism', types.StringType(), nullable=True),
    types.StructField('historic', types.StringType(), nullable=True),
    types.StructField('leisure', types.StringType(), nullable=True),
    types.StructField('shop', types.StringType(), nullable=True),
    types.StructField('name', types.StringType(), nullable=True),
    types.StructField('tags', types.MapType(types.StringType(), types.StringType()), nullable=False),
])


def get_amenities(line):
    root = etree.fromstring(line)
    if root.tag != 'node':
        return

    tags = {tag.get('k'): tag.get('v') for tag in root.iter('tag')}

    lat = float(root.get('lat'))
    lon = float(root.get('lon'))

    if not ((lon > -123.5) and (lon < -122) and \
        (lat > 49) and (lat < 49.5)):
        return

    if ('amenity' not in tags) and ('tourism' not in tags) and ('historic' not in tags) \
        and ('leisure' not in tags) and ('shop' not in tags):
        return

    if 'amenity' in tags:
        amenity = tags['amenity']
        del tags['amenity']
    else:
        amenity = None

    if 'name' in tags:
        name = tags['name']
        del tags['name']
    else:
        name = None

    if 'tourism' in tags:
        tourism = tags['tourism']
        del tags['tourism']
    else:
        tourism = None

    if 'historic' in tags:
        historic = tags['historic']
        del tags['historic']
    else:
        historic = None

    if 'leisure' in tags:
        leisure = tags['leisure']
        del tags['leisure']
    else:
        leisure = None

    if 'shop' in tags:
        shop = tags['shop']
        del tags['shop']
    else:
        shop = None

    yield Row(lat=lat, lon=lon, amenity=amenity, tourism=tourism, \
        historic=historic, leisure=leisure, shop=shop, name=name, tags=tags)


def main(inputs, output):
    lines = sc.textFile(inputs)
    nodes = lines.flatMap(get_amenities)
    amenities = spark.createDataFrame(nodes, schema=amenity_schema)
    # work around Python to Spark datetime conversion problems
    amenities = amenities.select(
        'lat', 'lon', 'amenity', 'tourism', 'historic', 'leisure', 'shop', 'name', 'tags'
    )
    amenities = amenities.cache()
    amenities.write.json(output, mode='overwrite', compression='gzip')


if __name__ == '__main__':
    inputs = sys.argv[1]
    output = sys.argv[2]
    main(inputs, output)

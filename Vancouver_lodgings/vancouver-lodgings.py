import os
import sys
from pyspark.sql import SparkSession, types
from pyspark.sql.dataframe import DataFrame
from pyspark.sql.functions import udf, array, lit, when, greatest
from geopy.distance import distance

spark = SparkSession.builder.appName('OSM point of interest extracter').getOrCreate()
assert spark.version >= '2.4' # make sure we have Spark 2.4+
spark.sparkContext.setLogLevel('WARN')


amenity_schema = types.StructType([
    types.StructField('lat', types.DoubleType(), nullable=False),
    types.StructField('lon', types.DoubleType(), nullable=False),
    types.StructField('amenity', types.StringType(), nullable=True),
    types.StructField('tourism', types.StringType(), nullable=True),
    types.StructField('historic', types.StringType(), nullable=True),
    types.StructField('leisure', types.StringType(), nullable=True),
    types.StructField('shop', types.StringType(), nullable=True),
    types.StructField('name', types.StringType(), nullable=True),
    # types.StructField('tags', types.MapType(types.StringType(), types.StringType()), nullable=False)
])


def main(input):
    data = spark.read.json(input, schema=amenity_schema)

    '''
    Concatanate latitude and longtitude
    Categorize the data 
    '''
    def getCoord():
        def _getCoord_(col1, col2):
            return (col1, col2)
        return udf(_getCoord_, types.ArrayType(types.FloatType()))

    data = data.withColumn('coord', getCoord()('lat', 'lon')).drop('lat', 'lon')

    amenity = data.filter(data['amenity'].isNotNull()).drop('tourism', 'historic', 'leisure', 'shop')
    tourism = data.filter(data['tourism'].isNotNull()).drop('amenity', 'historic', 'leisure', 'shop')
    historic = data.filter(data['historic'].isNotNull()).drop('amenity', 'tourism', 'leisure', 'shop')
    leisure = data.filter(data['leisure'].isNotNull()).drop('amenity', 'tourism', 'historic', 'shop')
    shop = data.filter(data['shop'].isNotNull()).drop('amenity', 'tourism', 'historic', 'leisure')

    '''
    Find places to stay (must have a name)
    '''
    named_tourism = tourism.filter(tourism['name'].isNotNull())

    hotel = named_tourism.filter(named_tourism['tourism'] == 'hotel').drop('tourism')
    motel = named_tourism.filter(named_tourism['tourism'] == 'motel').drop('tourism')
    hostel = named_tourism.filter(named_tourism['tourism'] == 'hostel').drop('tourism')
    guest_house = named_tourism.filter(named_tourism['tourism'] == 'guest_house').drop('tourism')
    apartment = named_tourism.filter(named_tourism['tourism'] == 'apartment').drop('tourism')
    house = named_tourism.filter(named_tourism['tourism'] == 'house').drop('tourism')

    '''
    Find places relavant to a trip's satisfaction 
    Give each place a score
    '''
    amenity_list = {'restaurant': 10, 'cafe': 10, 'bicycle_rental': 20, 'pub': 5, 'bar': 5, 'fuel': 1, 'vending_machine': 1,
    'place_of_worship': 1000, 'car_sharing': 5, 'ice_cream': 5, 'fountain': 1, 'theatre': 1000, 'car_rental': 5, 'marketplace': 2,
    'cinema': 20, 'nightclub': 10, 'art_centre': 10000, 'event_venue': 8000, 'boat_rental': 800, 'spa': 2000, 'juice_bar': 20,
    'park': 20, 'monestery': 4000, 'motorcycle_rental': 2, 'bistro': 5, 'casino': 20}
    tourism_list = {'viewpoint': 400, 'artwork': 100, 'museum': 10000, 'camp_site': 100, 'gallery': 5000, 'caravan_site': 500, 
    'winery': 4000, 'trail_riding_station': 2000, 'Ladner Harbour': 2000, 'Mid Valley Fisheries Enhancement': 1000, 'attraction': 4000}
    historic_list = {'memorial': 4000, 'monument': 4000, 'ruins': 6000, 'cannon': 4000, 'locomotive': 4000, 'aircraft': 4000,
    'castle': 10000, 'archaeological_site': 4000, 'church': 4000}
    leisure_list = {'fitness_centre': 10, 'sports_centre': 10, 'dance': 100, 'garden': 100, 'swimming_pool': 100, 'fishing': 1000, 
    'bowling_alley': 200, 'horse_riding': 200, 'water_park': 10000, 'golf_course': 100, 'nature_reserve': 1000, 'music_venue': 5000}
    shop_list = {'clothes': 5, 'hairdresser': 1, 'convenience': 2, 'gift': 10, 'beauty': 5, 'shoes': 5, 'jewelry': 5,
    'cosmestics': 5, 'art': 5, 'toys': 5}

    def mapToList(mapping):
        def _mapToList_(col):
            return mapping.get(col)
        return udf(_mapToList_, types.LongType())
    
    amenity = amenity.filter(amenity['amenity'].isin(list(amenity_list.keys())))
    amenity = amenity.withColumn('value', mapToList(amenity_list)('amenity'))
    tourism = tourism.filter(tourism['tourism'].isin(list(tourism_list.keys())))
    tourism = tourism.withColumn('value', mapToList(tourism_list)('tourism'))
    historic = historic.filter(historic['historic'].isin(list(historic_list.keys())))
    historic = historic.withColumn('value', mapToList(historic_list)('historic'))
    leisure = leisure.filter(leisure['leisure'].isin(list(leisure_list.keys())))
    leisure = leisure.withColumn('value', mapToList(leisure_list)('leisure'))
    shop = shop.filter(shop['shop'].isin(list(shop_list.keys())))
    shop = shop.withColumn('value', mapToList(shop_list)('shop'))

    '''
    Concatanate all places to visit 
    Double the value of named places
    Add a column of 1 (for later use)
    ''' 
    amenity = amenity.withColumnRenamed('amenity', 'type')
    tourism = tourism.withColumnRenamed('tourism', 'type')
    historic = historic.withColumnRenamed('historic', 'type')
    leisure = leisure.withColumnRenamed('leisure', 'type')
    shop = shop.withColumnRenamed('shop', 'type')

    location = amenity.union(tourism).union(historic).union(leisure).union(shop)
    location = location.withColumn('multiple', when(location['name'].isNotNull(), 2).otherwise(1))
    location = location.withColumn('value', location['value'] * location['multiple']).drop('multiple')
    location = location.withColumn('1', lit(1)).cache()

    '''
    Calculate the score of each place to stay
    Write them to csv file
    '''
    def getDistance():
        def _getDistance_(coord1, coord2):
            return distance(coord1, coord2).km
        return udf(_getDistance_, types.DoubleType())

    names = ['hotel', 'motel', 'hostel', 'guest_house', 'apartment', 'house']
    curr_path = os.path.abspath(os.getcwd())
    new_path = curr_path + '/lodging-scores/'
    if not os.path.exists(new_path):
        os.makedirs(new_path)

    i = 0
    places: DataFrame
    for places in [hotel, motel, hostel, guest_house, apartment, house]:
        scores = []
        print('calculating the score of every ' + names[i])

        for place in places.rdd.take(places.count()): 
            location2 = location.withColumn('coord2', array([lit(i) for i in place['coord']]))
            location2 = location2.withColumn('distance', getDistance()('coord', 'coord2'))
            location2 = location2.withColumn('score', location2['value'] / greatest(location2['distance'], location2['1']))
            score = location2.groupBy().sum('score').first()[0]
            scores.append({'name': place['name'], 'score': round(score)})

        scores = spark.createDataFrame(scores, schema=['name', 'score']).sort('score', ascending=False)
        scores.write.csv(new_path + names[i], 'overwrite')
        i += 1
       

if __name__ == '__main__':
    main(sys.argv[1])

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
import random


def main(input):
    # read the provided data and create a df called 'amenities'
    amenities = pd.read_json('amenities-vancouver.json.gz', lines = True)
    amenities = amenities.drop(columns = 'tags')

    amenities = amenities[(amenities['amenity'] == 'cafe') | (amenities['amenity'] == 'fast_food') | (amenities['amenity'] == 'bbq') |
                        (amenities['amenity'] == 'restaurant') | (amenities['amenity'] == 'bar') | (amenities['amenity'] == 'pub') |
                        (amenities['amenity'] == 'food_court')]
    amenities = amenities.reset_index(drop = True)

    # read the data of chain restaurants and create a df called 'restaurant_chains'
    restaurant_chains = pd.read_csv('restaurant_chains.csv')
    restaurant_chains = restaurant_chains.drop_duplicates(inplace=False).reset_index(drop = True)
    restaurant_chains = restaurant_chains.sort_values('title').reset_index(drop = True)

    chains_list = list(restaurant_chains['title'])

    def find_chain(name):
        if name in chains_list:
            return True
        else:
            return False  

    # split the original df into a df of chain restaurants and a df of non-chain restaurants
    chains = amenities[amenities['name'].apply(find_chain) == True].dropna().reset_index(drop = True)
    non_chains = amenities[amenities['name'].apply(find_chain) == False].dropna().reset_index(drop = True)


    # Part 1: Visulization of distribution and density of chains and non-chains
    # Figure 1
    plt.figure()
    plt.title('The scatter plot of chain restaurants in Vancouver')
    plt.xlabel('longitude')
    plt.ylabel('latitude')
    plt.scatter(chains['lon'], chains['lat'], c='r', s=5)
    plt.grid()
    plt.savefig('chains_scatter.png')
    #plt.show()

    # Figure 2
    plt.figure()
    plt.title('The scatter plot of non-chain restaurants in Vancouver')
    plt.xlabel('longitude')
    plt.ylabel('latitude')
    plt.scatter(non_chains['lon'], non_chains['lat'], c='c', s=5)
    plt.grid()
    plt.savefig('nonchains_scatter.png')
    #plt.show()

    # Figure 3
    plt.figure()
    plt.title('The scatter plot of chain and non-chain restaurants in Vancouver')
    plt.xlabel('longitude')
    plt.ylabel('latitude')
    plt.scatter(chains['lon'], chains['lat'], c='r', s=5)
    plt.scatter(non_chains['lon'], non_chains['lat'], c='c', s=5)
    plt.legend(['chains', 'non_chains'])
    plt.grid()
    plt.savefig('both_scatter.png')
    #plt.show()

    # Figure 4
    plt.figure()
    plt.title('The 2D histogram of chain restaurants in Vancouver')
    plt.xlabel('longitude')
    plt.ylabel('latitude')
    plt.hist2d(chains['lon'], chains['lat'])
    plt.colorbar()
    plt.savefig('chains_hist.png')
    #plt.show()

    # Figure 5
    plt.figure()
    plt.title('The 2D histogram of non-Chain restaurants in Vancouver')
    plt.xlabel('longitude')
    plt.ylabel('latitude')
    plt.hist2d(non_chains['lon'], non_chains['lat'])
    plt.colorbar()
    plt.savefig('nonchains_hist.png')
    #plt.show()


    # Part 2: compare the number of chains and non-chains in different selected regions
    def count_chains(lon1, lon2, lat1, lat2):
        df = chains[(chains['lon']>=lon1)&(chains['lon']<=lon2)&(chains['lat']>=lat1)&(chains['lat']<=lat2)]
        count = df.shape[0]
        return count

    def count_non_chains(lon1, lon2, lat1, lat2):
        df = non_chains[(non_chains['lon']>=lon1)&(non_chains['lon']<=lon2)&(non_chains['lat']>=lat1)&(non_chains['lat']<=lat2)]
        count = df.shape[0]
        return count
        
    chainsCount_list = []
    nonchainsCount_list = []
    lon1_list = [-123.4, -123.2, -123.2, -123.2, -123.2, -123.0, -123.0, -123.0, -122.8, -122.8, -122.8, -122.6, -122.6, -122.4, -122.4]
    lon2_list = [-123.2, -123.0, -123.0, -123.0, -123.0, -122.8, -122.8, -122.8, -122.6, -122.6, -122.6, -122.4, -122.4, -122.2, -122.2]
    lat1_list = [49.3, 49.0, 49.1, 49.2, 49.3, 49.0, 49.1, 49.2, 49.0, 49.1, 49.2, 49.0, 49.1, 49.0, 49.1] 
    lat2_list = [49.4, 49.1, 49.2, 49.3, 49.4, 49.1, 49.2, 49.3, 49.1, 49.2, 49.3, 49.1, 49.2, 49.1, 49.2]
    for i in range(len(lon1_list)):
        chainsCount_list.append(count_chains(lon1_list[i], lon2_list[i], lat1_list[i], lat2_list[i]))
        nonchainsCount_list.append(count_non_chains(lon1_list[i], lon2_list[i], lat1_list[i], lat2_list[i]))

    count_comparison = pd.DataFrame(list(zip(lon1_list, lon2_list, lat1_list, lat2_list, chainsCount_list, nonchainsCount_list)),
                columns =['lon1', 'lon2', 'lat1', 'lat2', 'num_chains', 'num_nonchains'])

    count_comparison.to_csv('count_comparison.csv', index=False)


    # Part 3: display the top 5 nearest entered restaurant names or restaurant types to the entered current location 
    # generator random current location
    input_lat = random.uniform(49.1, 49.3)
    input_lon = random.uniform(-123.2, -122.8)

    # reference: https://stackoverflow.com/questions/29545704/fast-haversine-approximation-python-pandas/29546836#29546836 
    # calculate distance (in meters) between two latitude/longitude points
    def haversine_dist(lon1, lat1, lon2, lat2):  

        lon1, lat1, lon2, lat2 = map(np.radians, [lon1, lat1, lon2, lat2])
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = np.sin(dlat/2.0)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon/2.0)**2
        c = 2 * np.arcsin(np.sqrt(a))
        dist = 6367 * c * 1000 # distance in m
        return dist

    # calculate the distance (in meters) between each latitude/longitude point in a dataframe with the input latitude/longitude point
    def distance(df, input_lon, input_lat):    
        lon1 = df.loc[0:, 'lon']
        lat1 = df.loc[0:, 'lat']
        lon2 = input_lon
        lat2 = input_lat
        dist_column = haversine_dist(lon1, lat1, lon2, lat2) 
        return dist_column

    chains['distance'] = distance(chains, input_lon, input_lat)
    chains['distance'] = round(chains['distance'], 2)

    # if the input is the restaruant name
    if input in chains_list: 
        input_name = input

        name_result = chains[chains['name'] == input_name].sort_values(by='distance', ascending=True).reset_index(drop = True)
        name_result = name_result.head()

        chains_labels = list(name_result['distance'])

        plt.figure(figsize=(12,8))
        plt.xlabel('longitude')
        plt.ylabel('latitude')
        plt.scatter([input_lon], [input_lat], c='b')

        plt.scatter(name_result['lon'], name_result['lat'], c='r')
        for i in range(len(chains_labels)):
            plt.annotate(chains_labels[i], (name_result['lon'][i], name_result['lat'][i]))

        plt.legend(['current location', input_name])
        plt.grid()
        plt.savefig('nearest_restaurant.png')
        #plt.show()
    # if the input is the restaruant type
    elif input in ['cafe', 'fast_food', 'bbq', 'restaurant', 'bar', 'pub', 'food_court']:
        input_amenity = input

        amenity_result = chains[chains['amenity'] == input_amenity].sort_values(by='distance', ascending=True).reset_index(drop = True)
        amenity_result = amenity_result.head()

        amenity_labels = []
        amenity_names = list(amenity_result['name'])
        amenity_dists = list(amenity_result['distance'])
        for i in range(len(amenity_names)):
            amenity_labels.append(amenity_names[i] + '\n' + str(amenity_dists[i]))
            
        plt.figure(figsize=(12,8))
        plt.xlabel('longitude')
        plt.ylabel('latitude')
        plt.scatter([input_lon], [input_lat], c='b')

        plt.scatter(amenity_result['lon'], amenity_result['lat'], c='r')
        for i in range(len(amenity_labels)):
            plt.annotate(amenity_labels[i], (amenity_result['lon'][i], amenity_result['lat'][i]))

        plt.legend(['current location', input_amenity])
        plt.grid()
        plt.savefig('nearest_amenity.png')
        #plt.show()
    else:
        print("please input valid restaruant namesor restaurant types\n")

if __name__ == '__main__':
    main(sys.argv[1])

import sys
import pandas as pd
import numpy as np
import geopandas as gpd
import matplotlib.pyplot as plt
import six
sys.modules['sklearn.externals.six'] = six
import mlrose
import requests
import json
import time

#References Used:
    #https://towardsdatascience.com/easy-steps-to-plot-geographic-data-on-a-map-python-11217859a2db
    #https://towardsdatascience.com/driving-distance-between-two-or-more-places-in-python-89779d691def
    #https://towardsdatascience.com/geopandas-101-plot-any-data-with-a-latitude-and-longitude-on-a-map-98e01944b972
    ##https://stackoverflow.com/a/40453439
    
#Stack Overflow
#https://stackoverflow.com/a/40453439
def haversine(lat1, lon1, lat2, lon2, earth_radius=6371):
    lat1 = np.radians(lat1)
    lat2 = np.radians(lat2)
    lon1 = np.radians(lon1)
    lon2 = np.radians(lon2)

    a = np.sin((lat2-lat1)/2.0)**2 + \
        np.cos(lat1) * np.cos(lat2) * np.sin((lon2-lon1)/2.0)**2

    return earth_radius * 2 * np.arcsin(np.sqrt(a))

#From the article:
#https://towardsdatascience.com/driving-distance-between-two-or-more-places-in-python-89779d691def
def get_distance(point1: dict, point2: dict) -> tuple:
    """Gets distance between two points en route using http://project-osrm.org/docs/v5.10.0/api/#nearest-service"""
    
    url = f"""http://router.project-osrm.org/route/v1/driving/{point1["lon"]},{point1["lat"]};{point2["lon"]},{point2["lat"]}?overview=false&alternatives=false"""
    r = requests.get(url)
    
    # get the distance from the returned values
    route = json.loads(r.content)["routes"][0]
    return (route["distance"])

def main(input):
    data = pd.read_parquet(input, engine='pyarrow')
    #Variables for binding the size of the plot
    lon_min = data['lon'].min()
    lon_max = data['lon'].max()
    lat_min = data['lat'].min()
    lat_max = data['lat'].max()

    #Modified version of the article
    #https://towardsdatascience.com/driving-distance-between-two-or-more-places-in-python-89779d691def
    dist_array = []
    for i , r in data.iterrows():
        point1 = {"lat": r["lat"], "lon": r["lon"]}
        for j, o in data[data.index != i].iterrows():
            point2 = {"lat": o["lat"], "lon": o["lon"]}
            #Haversine distance
            dist = haversine(point1['lat'],point1['lon'], point2['lat'], point2['lon']) #Haversine
            #OSM API distance
            #dist = get_distance(point1,point2) #OSM API
            dist_array.append((i, j, dist))
    
    fit_dist = mlrose.TravellingSales(distances=dist_array)
    problem_fit = mlrose.TSPOpt(length=data.shape[0], fitness_fn=fit_dist, maximize=False)
    #Use either optimized or less optimized algorithm
    #path, fitness = mlrose.genetic_alg(problem_fit, mutation_prob = 0.2,  max_attempts = 500, random_state = 2) #Optimized
    path, fitness = mlrose.genetic_alg(problem_fit, random_state = 2) #Not Optimized
    
    #Order DataFrame with calculated path
    order = {city: order for order, city in enumerate(path)}
    data['order'] = data.index.map(order)
    data = data.sort_values(by='order')

    #Plot the data onto a vancouver SHP file that shows the road data
    BBox = (lon_min, lon_max, lat_min, lat_max)
    map = gpd.read_file('gis_osm_roads_free_1.shp')
    fig,ax = plt.subplots(figsize=(100,100))
    ax.scatter(data['lon'], data['lat'], c='r', s=200, zorder=1)
    ax.plot(data['lon'], data['lat'], c='r', linewidth=7)
    #Set limits of plot plus a little more so edge geo locations are not right on the edge of the plot
    ax.set_xlim(BBox[0]-0.005, BBox[1]+0.005)
    ax.set_ylim(BBox[2]-0.005, BBox[3]+0.005)
    map.plot(ax=ax, zorder=0)
    print('Visit in this order\n',data['name'])
    print('It will take a total of', fitness, 'kilometres')
 

if __name__=='__main__':
    start_time = time.time()
    input = sys.argv[1] #Input is the OSM data to use from a parquet folder
    main(input)
    print("---%s seconds---" % (time.time()-start_time))
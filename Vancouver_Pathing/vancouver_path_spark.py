import sys
assert sys.version_info >= (3, 5) # make sure we have Python 3.5+
from pyspark.sql import SparkSession, functions, types

# add more functions as necessary

vancouver_schema = types.StructType([
    types.StructField('lat', types.DoubleType()),
    types.StructField('lon', types.DoubleType()),
    types.StructField('timestamp', types.StringType()),
    types.StructField('amenity', types.StringType()),
    types.StructField('name', types.StringType()),
    types.StructField('tags', types.StringType())
])

def main(inputs):
    data = spark.read.json(inputs, schema=vancouver_schema)
    data = data.where(data['tags'] != '{}').cache()
    data = data.filter((data['lon'] > -123.27) & (data['lon'] < -123.024))
    data = data.filter((data['lat'] > 49.2) & (data['lat'] < 49.31))
    wikidata = data.filter(data['tags'].contains('wikidata'))
    wikidata = wikidata.filter(~data['tags'].contains('brand'))
    wikidata = wikidata.filter(~data['amenity'].contains('school'))
    wikidata = wikidata.filter(~data['amenity'].contains('bench'))
    wikipedia = data.filter(data['tags'].contains('wikipedia'))
    wikipedia = wikipedia.filter(~data['tags'].contains('brand'))
    wikipedia = wikipedia.filter(~data['amenity'].contains('school'))
    wikipedia = wikipedia.filter(~data['amenity'].contains('library'))
    wikidata.write.parquet('wikidata', mode='overwrite')
    wikipedia.write.parquet('wikipedia', mode='overwrite')
    wikipedia.show()
    wikidata.show()

if __name__ == '__main__':
    inputs = sys.argv[1]
    spark = SparkSession.builder.appName('example code').getOrCreate()
    assert spark.version >= '3.1' # make sure we have Spark 3.1+
    spark.sparkContext.setLogLevel('WARN')
    #sc = spark.sparkContext

    main(inputs)
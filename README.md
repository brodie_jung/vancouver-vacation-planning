# CMPT353 Project

CMPT353 Project for Fall 2021
Project topic: Vancouver Vacation Planning

## Vancouver Routing:

There are two files to get the results described in the report

vancouver_path_spark.py and vancouver_path_pandas.py

Both of these files only take 1 input

For vancouver_path_spark the input is amenities-vancouver.json.gz

The output will be a wikipedia and wikidata parquet folder

For vancouver_path_pandas the input is which parquet file you want to read in

The output will be the time it took to run, the order to visit each location in, how many km's the path is, and a plot of the path on the vancouver shp file

First you run the vancouver_path_spark then vancouver_path_pandas as the output of vancouver_path_spark produces the input for vancouver_path_pandas

**Required Libraries**: mlrose, requests, json, time, geopandas, matplotlib.pyplot, six

Commands to run python files
```
spark-submit vancouver_path_spark.py amentities-vancouver.json.gz
python3 vancouver_path_pandas.py wikidata
python3 vancouver_path_pandas.py wikipedia
```


## Vancouver chain restaurants:

1. go to 'Vancouver_chains' directory, please make sure amenities-vancouver.json.gz and restaurant_chains.csv are always in the same directory as find_chains.py, so that find_chains.py can directly read those two files.

2. run the code by typing the following in the terminal:
   
   python find_chains.py input
   
   note: input can be two types: restaurant type or restaurant name
   
   (1) input restaurant type if you want to get the visualization of the nearest input restaurant type restaurant type should be one of the following: 'cafe', 'fast_food', 'bbq', 'restaurant', 'bar', 'pub', 'food_court'. For example,
   ```
   python find_chains.py fast_food
   ```
   (2) input restaurant name if you want to get the visualization of the nearest input restaurant name restaurant name should be one of the name in restaurant_chains.csv. open restaurant_chains.csv to find a valid name. For examples, 
   ```
   python find_chains.py Starbucks
   ```
* all the figures and tables in the report are generatored by running find_chains.py 


## Vancouver logdgings:

1. go to 'Vancouver_lodgings' directory (the data files are in 'vancouver-places' directory and the program is 'vancouver-lodgings.py')

2. (optional since vancouver-places folder is already included)
   Upload data_prep/vancouver-places.py to the cluster and connect to it
```
scp -P24 data_prep/vancouver-places.py [USERID]@cluster.cs.sfu.ca:
ssh -p24 <USERID>@cluster.cs.sfu.ca
```
   Then run vancouver-places.py on OSM dataset and copy the output from cluster's filesystem to local 
```
python vancouver-places.py /courses/datasets/openstreetmaps output
hdfs dfs -copyToLocal output
```
   Download the data files from the cluster to your computer
```
scp -P24 [USERID]@cluster.cs.sfu.ca:output vancouver-places
```
3. run the program using command (the running time is approx. 4 minutes on a M1 Macbook Air):
```
python vancouver-lodgings.py vancouver-places
```
4. look up the scores of the lodgings found in 'logdging-scores' directory

**Required Library**: geo


